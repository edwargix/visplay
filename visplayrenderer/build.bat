if exist build echo "build directory already exists, continuing..."

if not exist build mkdir build

cd build

qmake ..
nmake
