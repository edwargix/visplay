/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/QtWidgets/ClockHeader.h"

ClockHeader::ClockHeader(QWidget *parent,
                         QString headerText,
                         QString fontString,
                         int fontS,
                         QString headerStyleSheet, 
                         QString clockStyleSheet,
                         int clockWidth) : QWidget(parent)
{
    fontType = fontString;
    fontSize = fontS;
    QFont font(fontType, fontSize);

    QPointer<QHBoxLayout> layout = new QHBoxLayout(this);

    dclock = new DigitalClock(clockStyleSheet);
    dclock->setFixedWidth(clockWidth);

    header = new QLabel();
    header->setText(headerText);
    header->setStyleSheet(headerStyleSheet);
    header->setFont(font);

    layout->addWidget(header);
    layout->addWidget(dclock);

    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
}

void ClockHeader::setHeaderText(QString newText) {
    header->setText(newText);
}
QString ClockHeader::getHeaderText() {
    return header->text();
}
QTime ClockHeader::getTime() {
    return dclock->getClockTime();
}