/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/QtWidgets/DigitalClock.h"

DigitalClock::DigitalClock(int fontSize, QString fontType) : QLabel()
{
    _fontType = fontType;
    _fontSize = fontSize;
    QFont font(_fontType, _fontSize);
    setFont(font);

    setAlignment(Qt::AlignCenter);

    showTime();

    QPointer<QTimer> timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    timer->start(1000);
}
DigitalClock::DigitalClock(QString styleSheet, int fontSize, QString fontType) : DigitalClock(fontSize, fontType)
{
    setStyleSheet(styleSheet);
}

QTime DigitalClock::getClockTime()
{
    return _time;
}

void DigitalClock::showTime()
{
    _time = QTime::currentTime();
    QString text = _time.toString("h:mm AP");

    setText(text);
}