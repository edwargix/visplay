#!/bin/bash
mkdir -p build-arm
cd build-arm

if [ "$1" == "distclean" ]; then
    qmake-qt5 ../visplay-gui.pro
    make distclean
    cd ..
    rm -r build-arm
    exit
fi

export PATH=/usr/arm-linux-gnueabihf/bin:$PATH

whereis gcc

qmake-qt5 ../visplay-gui.pro
make -j5
