from azure.devops.connection import Connection
import os
import sys
from time import sleep

from msrest.authentication import BasicAuthentication

# Fill in with your personal access token and org URL
personal_access_token = os.environ['AZURE_TOKEN']
organization_url = 'https://dev.azure.com/' + os.environ['AZURE_ORG']
project_id = os.environ['AZURE_PROJECT_ID']
pipeline_id = os.environ['AZURE_PIPELINE_ID']

# Create a connection to the org
credentials = BasicAuthentication('', personal_access_token)
connection = Connection(base_url=organization_url, creds=credentials)
# Get a client (the "build" client provides access to build pipelines)
build_client = connection.clients.get_build_client()

# Create a build definition to send to the build_client
build = {'definition': {'id': pipeline_id}}

# Queue build
response = build_client.queue_build(build=build, project=project_id)

# Print assigned build id and url
print(str(response.id) + ": " + response.url)

sleep(1)

build = None

# Loop until build is completed
while True:
    build = build_client.get_build(build_id=response.id, project=project_id)
    if build.status == 'completed':
        break

# Print resulting build status
print(f"Build ID: {response.id}")
print(f"    Status: {build.status}")
print(f"    Result: {build.result}")
print("\n\n\n")

# Print build logs from all steps
logs = build_client.get_build_logs(build_id=response.id, project=project_id)
for log in logs:
    dl_log = build_client.get_build_log(
        build_id=response.id,
        project=project_id,
        log_id=log.serialize()['id'],
    )
    text = b"".join(dl_log)
    text_str = text.decode('utf-8')
    print(text_str)

if build.result != 'succeeded':
    sys.exit(-1)
else:
    # Download pipeline artifacts
    artifacts = build_client.get_artifacts(
        build_id=response.id,
        project=project_id,
    )
    for artifact in artifacts:
        artifact_contents = build_client.get_artifact_content_zip(
            build_id=response.id,
            project=project_id,
            artifact_name=artifact.name)

        print(
            f'Downloading pipeline artifact {artifact.name} to',
            './cicd/azure_forwarding/artifacts/{artifact.name}.zip',
        )

        with open(
                f'./cicd/azure_forwarding/artifacts/{artifact.name}.zip',
                'wb',
        ) as output_file:
            for chunk in artifact_contents:
                output_file.write(chunk)

        sys.exit(0)
