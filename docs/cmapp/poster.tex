%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This template has been modified by:
%       Jonathan Sumner Evans (jonathanevans@mines.edu)
%       Sam Sartor (ssartor@mines.edu)
%       Robbie Merillat (rdmerillat@mines.edu)


%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[scale=1.2]{beamerposter} % Use the beamerposter package for laying out the poster
\usepackage{csquotes}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{booktabs} % Top and bottom rules for tables
\usepackage[export]{adjustbox}
\usepackage{parskip}

\usetheme{confposter} % Use the confposter theme supplied with this template

\setbeamercolor{block title}{fg=ngreen,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\setlength{\paperwidth}{48in} % A0 width: 46.8in
\setlength{\paperheight}{36in} % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.3\paperwidth} % Width of one column
\setlength{\twocolwid}{0.6\paperwidth} % Width of two columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size

%-----------------------------------------------------------

\title{Visplay --- Distributed Digital Signage Protocol and Implementation}
\author{Mines ACM Visplay Team}
\institute{Department of Computer Science, Colorado School of Mines}

\begin{document}

    \addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
    \addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

    \setlength{\belowcaptionskip}{2ex} % White space under figures
    \setlength\belowdisplayshortskip{2ex} % White space under equations

    \begin{frame}[t] % The whole poster is enclosed in one beamer frame
        % The whole poster consists of three major columns, the second of which
        % is split into two columns twice - the [t] option aligns each column's
        % content to the top
        \begin{columns}[t]
            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\onecolwid} % The first column

                \begin{block}{Motivation}
                    \setlength{\parskip}{0.5em}

                    You may have seen digital signage on TVs around campus
                    showing weather, news, events and other information. Most of
                    these systems are proprietary and expensive, so Mines ACM
                    decided to create an Open Source project which could be used
                    for digital signage.

                    In addition to creating an Open Source alternative to the
                    systems in use currently, we decided to design it in a way
                    that is scalable, and protocol oriented.
                \end{block}

                \begin{block}{Protocol Design}
                    \setlength{\parskip}{0.5em}

                    We spent much of the 2017/18 school year designing an
                    effective protocol specification for Visplay. During this
                    time, we iterated and improved our design constantly.

                    Towards the end of last year, we stabilized our protocol
                    design.

                    \begin{figure}[H]
                        \centering
                        \includegraphics[width=\textwidth]{../graphics/architecture}
                        \caption{Protocol Design}\label{fig:protocol}
                    \end{figure}

                    The main features of this design are its hierarchical
                    nature, and flexible structure.
                \end{block}

                \setbeamercolor{block alerted title}{fg=black,bg=norange} % Change the alert block title colors
                \setbeamercolor{block alerted body}{fg=black,bg=white} % Change the alert block body colors

                \begin{alertblock}{More Information}

                    \begin{itemize}

                        \item \textbf{Project:} \\
                            \href{https://gitlab.com/ColoradoSchoolOfMines/visplay}{\url{gitlab.com/ColoradoSchoolOfMines/visplay}}

                        \item \textbf{Documentation:} \\
                            \href{https://coloradoschoolofmines.gitlab.io/visplay/}{\url{coloradoschoolofmines.gitlab.io/visplay/}}
                    \end{itemize}

                \end{alertblock}

            \end{column} % End of the first column

            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\onecolwid} % Start of column 2
                \setlength{\parskip}{0.5em}

                \begin{block}{Goals for 2018/19}
                    \setlength{\parskip}{0.5em}

                    \begin{itemize}

                        \item \textbf{Deploy} Visplay to at least one TV on
                            campus.

                        \item \textbf{Enable auto-updating configuration} on the
                            server.

                        \item \textbf{Create a configuration GUI} for easy
                            configuration of assets and playlists.

                        \item \textbf{Document} our program architecture and
                            code.

                        \item \textbf{Implement an automated build system} for
                            automatically compiling and testing commits.

                        \item \textbf{Command and control system} for remotely
                            managing displays.

                    \end{itemize}

                    We explore our progress on these goals in the following
                    sections.

                    % A working server that pulls configurations from upstream to
                    % create a playlist

                    % Polling for config changes in the background. Ability to
                    % upgrade to WebSockets connection if supported by client and
                    % server

                    % C\&C protocol, Web C\&C GUI

                \end{block}

                \begin{block}{Deploy Visplay}
                    \setlength{\parskip}{0.5em}

                    In order to achieve our goal of deploying Visplay, we
                    stabilized the graphical output component,
                    \texttt{visplayrenderer}. This was the last component that
                    needed to be stabilized for us to begin testing Visplay in a
                    production environment.

                    Our primary goal for the renderer architecture was that it
                    could be run on any hardware, from a Raspberry Pi to an
                    Intel NUC.

                    In the future, we intend to create comprehensive images for
                    deploying Visplay on various hardware. We also would like to
                    create physical units for on-campus use.

                \end{block}

                \begin{block}{Auto-Updating Configuration}
                  \setlength{\parskip}{0.5em}

                  Currently, screens receive playlist configurations from the
                  Visplay Servers, however, after deployment, the sign's list of
                  videos needs to be kept up to date. Thanks to Visplay's
                  automatically updating configuration system, these can be
                  updated seamlessly with zero downtime.

                  Visplay uses Python's Watchdog to watch the filesystem and
                  Python's Asyncio to watch online resources. Whenever a change
                  is detected, Visplay reloads the necessary files and begins
                  playing the new content immediately.

                \end{block}

                \begin{block}{Technologies Used}
                    \begin{enumerate}[leftmargin=8cm, labelsep=1cm]
                        \item[\textbf{Python}] for our backend servers, and
                            client control code.

                        \item[\textbf{React JS}] for our frontend configuration
                            generator.

                        \item[\textbf{Qt}] for configuring the graphical output
                            to the TVs.

                        \item[\textbf{mpv}] for rendering videos on the TVs.

                        \item[\textbf{HTTP(S)}] for inter-process and
                            inter-machine communication.

                        \item[\textbf{YAML}] for specifying asset, playlist, and
                            device configuration.
                    \end{enumerate}
                \end{block}

            \end{column} % End of column 2

            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\onecolwid} % Start of third column

                \begin{block}{Configuration GUI}
                    \setlength{\parskip}{0.5em}

                    Over the course of the past semester, we have implemented a
                    web-based user interface for adding, removing, and editing
                    the assets to be shown by the Visplay clients.

                    Much of our time was spent laying the foundation for the
                    application. We chose to use React JS to implement a
                    single-page application.

                    Our next goal is to implement a playlist editor.

                    \hspace{1em}

                    \begin{figure}[H]
                        \centering
                        \includegraphics[width=0.77\textwidth,frame]{../graphics/screenshots/config-generator}
                        \caption{Configuration Generator Screenshot}
                        \label{fig:config-generator}
                    \end{figure}

                \end{block}

                \begin{block}{Documentation and CI/CD}
                    \setlength{\parskip}{0.5em}

                    We implemented a build and test system using GitLab CI/CD so
                    that we can develop and iterate with confidence. Our build
                    process automatically builds, tests, and lints our Python,
                    JavaScript, and C++ code. The build pipeline also
                    automatically builds and deploys our documentation.

                    To speed up the build process, we also developed a custom
                    Docker container with all of the resources that we need to
                    build Visplay.

                \end{block}

                \begin{block}{Future Goals}
                    \setlength{\parskip}{0.5em}

                    \begin{itemize}
                        \item \textbf{Command and Control:} We would like to
                            have the ability to control the TVs running Visplay
                            from the central server.

                        \item \textbf{Server-Level Caching:} We would like to
                            cache certain resources such as YouTube videos on
                            the local network to reduce out-of-network data
                            transfer.

                        \item \textbf{Improved Hardware Setup:} We would like to
                            streamline the setup process for new Visplay
                            Clients.

                        \item \textbf{Authentication Protected Assets:} We would
                            like to create a structure for separating public and
                            private assets.
                    \end{itemize}

                \end{block}

                \begin{columns}[t]
                    \begin{column}{0.4\textwidth}
                        \includegraphics[width=\textwidth]{graphics/csam}
                    \end{column}
                    \begin{column}{0.4\textwidth}
                        \includegraphics[width=\textwidth]{graphics/dept-of-cs}
                    \end{column}
                \end{columns}

            \end{column} % End of the third column

        \end{columns} % End of all the columns in the poster

    \end{frame} % End of the enclosing frame

\end{document}
