VisplyRenderer
==============


Building
--------

To build VisplayRenderer, the required dependenices must be installed.

Once installed, ``cd`` into the VisplayRenderer directory.

Next, run the build script (``build.sh;` for Linux and ``build.bat`` for Windows)


Running
-------

After compiling, run the ``visplayrenderer`` binary in the build directory.

To control VisplayRenderer using curl, use the following commands.

``curl -H "Content-Type: application/json" -X POST -d '{"layout":4, "header":"", "path":"https://www.youtube.com/watch?v=hHW1oY26kxQ"}' http://localhost:25565/backbuffer/change_layout``
``curl -H "Content-Type: application/json" -X POST -d '{"path":"https://www.youtube.com/watch?v=hHW1oY26kxQ"}' http://localhost:25565/swap_buffers``

