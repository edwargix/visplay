Windows 10
==========

Qt 5
----
We will need to install and setup the development environment for Qt 5.

The Qt 5 installer can be found here__.

.. __: https://www.qt.io/download-qt-installer

Once the installer is downloaded:

    1. Run the downloaded exe file.

    2. When the installer asks you to login or make an account, click the skip
       option in the bottom right.

    3. Continue through the installer until you are prompted for an install
       location.
           
            - For this location use ``C:\Qt``

    4. Next you will have to decide which Qt version and what optional features
       you want to install.
           
           - Expand the options under the latest version of Qt 5 (5.13.0 at the
             time of this writing) and check the box next to MSVC 2017 64-bit.

           - Uncheck all other boxes (note that the Developer and designer
             tools will still remain with a black box)

    5. Continue through the installer until the installation completes
       (this may take awhile so make sure to do this with a decent internet
       connection and AC power)


MSVC 2017
---------
MSVC (Microsoft Visual C++) 2017 can be installed using the MSVC 2019
installer which can be found here__.

.. __: https://visualstudio.microsoft.com/vs/community

Once the installer is downloaded:

    1. Run the downloaded exe file.

    2. Run through the installer until you are presented with the
       versions of Visual Studio that are available. 

    3. Under Visual Studio Community 2019 select "More"->"Modify"

    4. Select the tab at the top that say "Individual components"

    5. Scroll down until you see the section titled "Compilers, build tools,
       and runtimes". Under this header select "MSVC v141 - VS 2017 C++
       x64/x86 build tools (v14.16)"

    6. Click "install" in the bottom right and let the installation complete

Windows 8.1 SDK
---------------
We will need to install the Windows 8.1 SDK to build visplayrenderer. 

The Windows 8.1 SDK installer can be found here__. You will have to scroll down
to the earlier releases section to find it. 

.. __: https://developer.microsoft.com/en-us/windows/downloads/sdk-archive

Once the installer is downloaded:

    1. Run the downloaded exe file.

    2. Continue you through the installer until you reach the
       "Select the features you want to download" menu.
        
           - Uncheck all the boxes except for "Windows Software Development Kit"

    3. Click "Download" and wait for the installation to complete.


QtWebkit
--------

qhttpengine
-----------
