const webpack = require('webpack');
const dotenv = require('dotenv');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const sourceFolder = 'frontend/src';

module.exports = () => {
  const basePath = './frontend/.env';

  const fileEnv = dotenv.config({path: basePath}).parsed || {};

  return {
    entry: `./${sourceFolder}/index.js`,
    output: {
      path: path.join(__dirname, '/dist'),
      filename: 'index_bundle.js',
    },
    devServer: {
      contentBase: './dist',
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.API_URL': JSON.stringify(fileEnv['API_URL']),
      }),
      new HtmlWebpackPlugin({
        template: `./${sourceFolder}/index.html`,
      }),
    ],
  };
};
